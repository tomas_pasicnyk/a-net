<?php

namespace App\Model;

use Nette,
	dibi;

class BaseModel extends Nette\Object
{
	public function __construct()
	{
		dibi::connect(array(
			'driver'   => 'mysql',
			'host'     => 'localhost',
			'username' => 'root',
			'password' => '',
			'database' => 'a-net',
			'charset'  => 'utf8',
		));
	}
}
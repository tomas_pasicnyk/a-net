<?php

namespace App\Model;
use dibi;

class UserModel extends BaseModel
{
	public function insert($data)
	{
		dibi::query('INSERT INTO user', $data);
		
		return dibi::getInsertId();
	}
	
	public function update($data)
	{
		dibi::query('UPDATE user SET', $data, 'WHERE user_id = %i', $data['user_id']);
	}
	
	public function delete($userId)
	{
		dibi::query('DELETE FROM user WHERE user_id = %i', $userId);
	}
		
	public function getUsers()
	{
		return dibi::query('SELECT * FROM user')->fetchAll();
	}
	
	public function getUserById($userId)
	{
		return dibi::query('SELECT * FROM user WHERE user_id = %i', $userId)->fetch();
	}
}
<?php

namespace App\Presenters;

use Nette;
use App\Model;


class HomepagePresenter extends BasePresenter
{
	public function renderDefault()
	{		
		if($this->user->isLoggedIn())
		{
			$this->template->content = 'Tento text vidí pouze přihlášení uživatelé';
		}
    }	
}

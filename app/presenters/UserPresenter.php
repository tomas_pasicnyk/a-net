<?php

namespace App\Presenters;

use Nette;
use App\Model,
	\Nette\Application\UI\Form;


class UserPresenter extends BasePresenter
{	
	private $userModel;
	
	private $userDetail;
	private $users;
	
	protected function beforeRender()
    {
        parent::beforeRender();
		
		if(!$this->user->isLoggedIn() || !$this->user->isInRole('admin'))
		{
			$this->flashMessage('Pro vstup do správy uživatelů se musíte přihlásit jako administrátor.');
			$this->redirect('Sign:in', array('backlink' => $this->storeRequest()));
		}
    }
	
	public function actionDefault()
	{
		$this->users = $this->userModel->getUsers();
	}
	
	public function renderDefault()
	{
		$this->template->users = $this->users;
	}
	
	public function actionDetail($userId)
	{
		$this->userDetail = $this->userModel->getUserById($userId);
	}
	
	public function renderDetail($userId)
	{
		$this->template->userDetail = $this->userDetail;
	}
	
	public function handleInsertNewUser()
	{
		$data = array('user_login' => '');
		$user = $this->userModel->insert($data);
				
		$this->redirect('User:detail', $user);
	}
	
	public function handleDeleteUser($userId)
	{
		$this->userModel->delete($userId);
		
		$this->redirect('User:');
	}
	
	public function createComponentUserUpdateForm()
	{
		$form = new Form;
		
		$form->addHidden('user_id');
		
		$form->addText('user_name', 'Jméno a příjmení: ')
			->addRule(Form::FILLED, 'Zadejte prosím jméno a příjmení: ');		
		$form->addText('user_login', 'Login: ')
			->addRule(Form::FILLED, 'Zadejte prosím uživatelské jméno: ');
		$form->addText('user_email', 'Email: ')
			->addRule(Form::EMAIL, 'Zadejte prosím e-mail ve správném formátu.');		
		$form->addPassword('user_password', 'Heslo: ')
			->addRule(Form::FILLED, 'Zadejte prosím heslo.');
		
		$form->addSubmit('insert', 'Uložit');
		
		$form->setDefaults($this->userDetail);
		$form->onSuccess[] = $this->userUpdateFormSuccess;
		
		return $form;
	}
	
	public function userUpdateFormSuccess(Form $form, $values)
    {	
		$values['user_password'] = Nette\Security\Passwords::hash($values->user_password);
		$this->userModel->update($values);
		
		$this->flashMessage('Uživatel byl úspěšně uložen', 'success');
		$this->redirect('User:');
	}
	
	/**
     * @param Model\UserModel
     */
    public function injectModels(Model\UserModel $userModel)
    {
        $this->userModel = $userModel;
    }

}

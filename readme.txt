Popis řešení
=============

Jednoduchá aplikace na správu uživatelů. Na úvodní straně se nachází odkazy a možností přihlášení.
Pouze přihlášeným uživatelům se zobrazí text navíc. 

Do sekce uživatelé se dostanou pouze přihlášení uživatelé s rolí "admin", ostatní to přesměruje na přihlášení.

V sekci uživatelé se nachází seznam přidaným uživatelů a možnost editace, přidání a mazání.

Přes composer jsem si stáhl Nette sandbox a Dibi.

V repositáři je i import na DB tabulku USER(user.sql), která obsahuje několik uživatelů, včetně admina. Hesla jsou hashovaná.

Login admina je "admin" a heslo "admin123"
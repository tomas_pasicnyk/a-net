-- Adminer 4.2.0 MySQL dump

SET NAMES utf8mb4;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `user_login` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `user_email` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `user_password` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `user_role` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `user` (`user_id`, `user_name`, `user_login`, `user_email`, `user_password`, `user_role`) VALUES
(23,	'Tomáš Pasičnyk',	'thomas',	'tomas@pasicnyk.com',	'$2y$10$XRTS9kffqY3/OgHjhXe50.xOs7M70PUVf7OyjfT3OWnSILUduhVIe',	''),
(25,	'Tomáš',	'admin',	'admin@admin.com',	'$2y$10$HjGT5ddW2x0lMD6QiKMbBuOyTWiZRGW/z2Tail5TVcO6u8CjDgos.',	'admin'),
(26,	'Pepa Uživatel',	'pepa',	'pepa@pepa.cz',	'$2y$10$M4rR4A1LtasyLntVKYSzWuqrNiwTk9C6BX7xgni1Oy3vkds.yDHEe',	'');

-- 2015-10-08 10:07:07
